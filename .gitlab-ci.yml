---
stages:
  - pre-commit
  - test
  - publish

include:
  # pre-commit
  - project: saltstack/pop/cicd/ci-templates
    file: /lint/pre-commit-run-all.yml
  # publish
  - project: saltstack/pop/cicd/ci-templates
    file: /docker/kaniko-single-combined.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /release/pop_release.yml

pre-commit-run-all:
  image: registry.gitlab.com/saltstack/pop/cicd/containers/alpinelint:py39

variables:
  CICD_UPSTREAM_PATH: saltstack/pop/tiamat

.test:
  stage: test
  needs:
    - pre-commit-run-all
  script:
    # add pyenv
    - git clone https://github.com/pyenv/pyenv.git ~/.pyenv
    - echo "export PYENV_ROOT='/root/.pyenv'" >> ~/.bashrc
    - echo "export PATH='/root/.pyenv/bin:$PATH'" >> ~/.bashrc
    - source ~/.bashrc
    - git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
    - python3 -m pip install nox
    - nox -e tests-3
  artifacts:
    when: always
    paths:
      - artifacts
    reports:
      junit: artifacts/junit-report.xml
    expire_in: 30 days

tests-3.9:
  extends: .test
  image: python:3.9

tests-3.10:
  extends: .test
  image: python:3.10.2

docs-build:
  stage: test
  image: python:3.10
  script:
    - python3 -m pip install nox
    - nox -e 'docs-html(compress=False)'
    - mv docs/_build/html ./html
  artifacts:
    paths:
      - html
    expire_in: 30 days

pop-release-tag:
  stage: publish

pages:
  stage: publish
  dependencies:
    - docs-build
  image: alpine
  script:
    - mv html public
    - 'echo "Gitlab Pages available at: ${CI_PAGES_URL}"'
  artifacts:
    paths:
      - public
    expire_in: 30 days

# Tiamat docker image builds
tiamat-staticx:
  stage: publish
  dependencies: []
  extends: .kaniko-build
  variables:
    CONTAINER_NAME: tiamat
    CONTAINER_TAG: latest,staticx,staticx-${CI_COMMIT_REF_SLUG}
    CONTEXT_PATH: ./
    DOCKERFILE_PATH: docker/tiamat-staticx.Dockerfile
    KANIKO_DOCKERHUB_NAMESPACE: saltstack

tiamat-amd64:
  stage: publish
  dependencies: []
  extends: .kaniko-build
  variables:
    CONTAINER_NAME: tiamat
    CONTAINER_TAG: amd64,amd64-${CI_COMMIT_REF_SLUG}
    CONTEXT_PATH: ./
    DOCKERFILE_PATH: docker/tiamat-amd64.Dockerfile
    KANIKO_DOCKERHUB_NAMESPACE: saltstack

tiamat-win64:
  stage: publish
  extends: .kaniko-build
  variables:
    CONTAINER_NAME: tiamat
    CONTAINER_TAG: win64,win64-${CI_COMMIT_REF_SLUG}
    CONTEXT_PATH: ./
    DOCKERFILE_PATH: docker/tiamat-win64.Dockerfile
