from unittest import mock


def test_report(hub, mock_hub, bname):
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].name = "test_name"
    mock_hub.tiamat.post.report = hub.tiamat.post.report
    with mock.patch("builtins.print") as mock_print:
        mock_hub.tiamat.post.report(bname)
        mock_print.assert_called_once()


def test_clean(hub, mock_hub, bname):
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].dir = "/tmp/test_name"
    mock_hub.tiamat.BUILDS[bname].name = "test_name"
    mock_hub.tiamat.BUILDS[bname].spec = "test.spec"
    mock_hub.tiamat.BUILDS[bname].req = "requirements.txt"
    mock_hub.tiamat.BUILDS[bname].venv_dir = "test_name"
    mock_hub.builder.pyinstaller.clean = hub.builder.pyinstaller.clean
    with mock.patch("shutil.rmtree") as sr, mock.patch("os.remove") as osr:
        mock_hub.builder.pyinstaller.clean(bname)
        sr.assert_called_once()
        osr.assert_called()
