"""
Test the build process and execute the resulting binary to ensure correct output.
"""
from unittest import mock


def test_build_pyenv(hub, pb_bin, data_dir):
    pb_conf = str(data_dir / "pb" / "pb-pyenv.yml")
    with mock.patch(
        "sys.argv",
        [
            "tiamat/run.py",
            "--log-level=debug",
            "build",
            "--config",
            pb_conf,
            "--exclude",
            "aiologger",
            "aiofiles",
            "pycparser",
            "pyinstaller",
            "pyinstaller-hooks-contrib",
            "setuptools",
            "wheel",
            "--pyinstaller-args",
            "'--strip'",
        ],
    ):
        hub.tiamat.init.cli()

    cp = hub.tiamat.cmd.run(pb_bin)

    assert not cp.retcode, cp.stderr
    assert "pb works!" in cp.stdout
    assert "hashids works!" in cp.stdout
