import shutil
from unittest import mock

import pytest


@pytest.mark.skipif(not shutil.which("fpm"), reason="fpm is required to run this test")
@pytest.mark.expensive_test
def test_pkgbuild(hub, pb_bin, pb_conf):
    with mock.patch(
        "sys.argv",
        [
            "tiamat/run.py",
            "--log-level=debug",
            "build",
            "--config",
            pb_conf,
            "--pkg-tgt",
            "arch",
            "--exclude",
            "aiologger",
            "pycparser",
            "pyinstaller",
            "pyinstaller-hooks-contrib",
            "setuptools",
            "wheel",
            "--pyinstaller-args",
            "'--strip'",
        ],
    ):
        hub.tiamat.init.cli()

    cp = hub.tiamat.cmd.run(pb_bin)

    assert not cp.retcode, cp.stderr
    assert "pb works!" in cp.stdout
    assert "hashids works!" in cp.stdout
    assert "bodger" in cp.stdout
